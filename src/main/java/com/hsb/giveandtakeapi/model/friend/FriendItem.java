package com.hsb.giveandtakeapi.model.friend;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FriendItem {
    private Long id;
    private String name;
    private String phoneNumber;
    private Boolean cutCheck;
    private LocalDate cutDate;
    private String cutReason;
}
