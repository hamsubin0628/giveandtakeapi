package com.hsb.giveandtakeapi.model.friend;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FriendCutUpdateRequest {
    private String cutReason;
}
