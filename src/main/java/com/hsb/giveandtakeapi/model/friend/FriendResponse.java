package com.hsb.giveandtakeapi.model.friend;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FriendResponse {
    private Long id;
    private String name;
    private String cutCheck;
}
