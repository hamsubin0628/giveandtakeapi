package com.hsb.giveandtakeapi.model.giveAndTake;

import com.hsb.giveandtakeapi.enums.GiftType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class GiveAndTakeCreateRequest {
    private GiftType giveType;
    private String giftContent;
    private Double giftPrice;
    private LocalDate giftDate;
}
