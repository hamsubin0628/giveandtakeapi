package com.hsb.giveandtakeapi.model.giveAndTake;

import com.hsb.giveandtakeapi.enums.GiftType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class GiveAndTakeItem {
    private Long friendId;
    private String giveTypeName;
    private String giftContent;
    private Double giftPrice;
    private LocalDate giftDate;
}
