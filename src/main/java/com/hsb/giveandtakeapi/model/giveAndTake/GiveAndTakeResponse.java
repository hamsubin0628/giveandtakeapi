package com.hsb.giveandtakeapi.model.giveAndTake;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class GiveAndTakeResponse {
    private Long friendId;
    private String giveTypeName;
    private String giftContent;
    private Double giftPrice;
}
