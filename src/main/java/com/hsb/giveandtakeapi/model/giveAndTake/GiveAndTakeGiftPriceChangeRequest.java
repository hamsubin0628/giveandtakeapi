package com.hsb.giveandtakeapi.model.giveAndTake;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GiveAndTakeGiftPriceChangeRequest {
    private Double giftPrice;
}
