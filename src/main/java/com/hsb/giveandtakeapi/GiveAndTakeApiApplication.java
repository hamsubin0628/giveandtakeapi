package com.hsb.giveandtakeapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GiveAndTakeApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GiveAndTakeApiApplication.class, args);
	}

}
