package com.hsb.giveandtakeapi.service;

import com.hsb.giveandtakeapi.entity.Friend;
import com.hsb.giveandtakeapi.model.friend.FriendCreateRequest;
import com.hsb.giveandtakeapi.model.friend.FriendItem;
import com.hsb.giveandtakeapi.model.friend.FriendCutUpdateRequest;
import com.hsb.giveandtakeapi.model.friend.FriendResponse;
import com.hsb.giveandtakeapi.repository.FriendRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FriendService {
    private final FriendRepository friendRepository;

    public Friend getData(long id){
        return friendRepository.findById(id).orElseThrow();
    }


    public void setFriend(FriendCreateRequest request){
        Friend addData = new Friend();
        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setCutCheck(request.getCutCheck());
        addData.setCutDate(LocalDate.now());
        addData.setCutReason(request.getCutReason());

        friendRepository.save(addData);
    }

    public List<FriendItem> getFriends(){
        List<Friend> originList = friendRepository.findAll();

        List<FriendItem> result = new LinkedList<>();

        for(Friend friend : originList){
            FriendItem addItem = new FriendItem();
            addItem.setId(friend.getId());
            addItem.setName(friend.getName());
            addItem.setPhoneNumber(friend.getPhoneNumber());
            addItem.setCutCheck(friend.getCutCheck());
            addItem.setCutDate(friend.getCutDate());
            addItem.setCutReason(friend.getCutReason());

            result.add(addItem);
        }
        return result;
    }

    public FriendResponse getFriend(long id){
        Friend originData = friendRepository.findById(id).orElseThrow();

        FriendResponse response = new FriendResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setCutCheck(originData.getCutCheck()? "손절":"손절안함");

        return response;
    }

    public void putFriend(long id, FriendCutUpdateRequest request){
        Friend originData = friendRepository.findById(id).orElseThrow();
        originData.setCutCheck(true);
        originData.setCutDate(LocalDate.now());
        originData.setCutReason(request.getCutReason());

        friendRepository.save(originData);
    }

    public void putFriendCancel(long id, FriendCreateRequest request){
        Friend originData = friendRepository.findById(id).orElseThrow();
        originData.setCutCheck(false);
        originData.setCutReason(null);
        originData.setCutReason(null);

        friendRepository.save(originData);
    }
}
