package com.hsb.giveandtakeapi.service;

import com.hsb.giveandtakeapi.entity.Friend;
import com.hsb.giveandtakeapi.entity.GiveAndTake;
import com.hsb.giveandtakeapi.model.giveAndTake.GiveAndTakeCreateRequest;
import com.hsb.giveandtakeapi.model.giveAndTake.GiveAndTakeGiftPriceChangeRequest;
import com.hsb.giveandtakeapi.model.giveAndTake.GiveAndTakeItem;
import com.hsb.giveandtakeapi.model.giveAndTake.GiveAndTakeResponse;
import com.hsb.giveandtakeapi.repository.GiveAndTakeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GiveAndTakeService {
    private final GiveAndTakeRepository giveAndTakeRepository;

    public void setGiveAndTake(Friend friend, GiveAndTakeCreateRequest request){
        GiveAndTake addData = new GiveAndTake();
        addData.setFriend(friend);
        addData.setGiveType(request.getGiveType());
        addData.setGiftContent(request.getGiftContent());
        addData.setGiftPrice(request.getGiftPrice());
        addData.setGiftDate(LocalDate.now());

        giveAndTakeRepository.save(addData);
    }

    public List<GiveAndTakeItem> getGiveAndTakes(){
        List<GiveAndTake> originList = giveAndTakeRepository.findAll();

        List<GiveAndTakeItem> result = new LinkedList<>();

        for(GiveAndTake giveAndTake : originList){
            GiveAndTakeItem addItem = new GiveAndTakeItem();
            addItem.setFriendId(giveAndTake.getFriend().getId());
            addItem.setGiveTypeName(giveAndTake.getGiveType().getGiftTypeEnum());
            addItem.setGiftContent(giveAndTake.getGiftContent());
            addItem.setGiftPrice(giveAndTake.getGiftPrice());
            addItem.setGiftDate(giveAndTake.getGiftDate());

            result.add(addItem);
        }
        return result;
    }

    public GiveAndTakeResponse getGiveAndTake(long id){
        GiveAndTake originData = giveAndTakeRepository.findById(id).orElseThrow();

        GiveAndTakeResponse response = new GiveAndTakeResponse();
        response.setFriendId(originData.getFriend().getId());
        response.setGiveTypeName(originData.getGiveType().getGiftTypeEnum());
        response.setGiftContent(originData.getGiftContent());
        response.setGiftPrice(originData.getGiftPrice());

        return response;
    }

    public void putGiveAndTake(long id, GiveAndTakeGiftPriceChangeRequest request){
        GiveAndTake originData = giveAndTakeRepository.findById(id).orElseThrow();
        originData.setGiftPrice(request.getGiftPrice());

        giveAndTakeRepository.save(originData);
    }
}
