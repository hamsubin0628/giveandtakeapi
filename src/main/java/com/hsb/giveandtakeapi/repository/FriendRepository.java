package com.hsb.giveandtakeapi.repository;

import com.hsb.giveandtakeapi.entity.Friend;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FriendRepository extends JpaRepository<Friend, Long> {
}
