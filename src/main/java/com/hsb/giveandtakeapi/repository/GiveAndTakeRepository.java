package com.hsb.giveandtakeapi.repository;

import com.hsb.giveandtakeapi.entity.GiveAndTake;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GiveAndTakeRepository extends JpaRepository<GiveAndTake, Long> {
}
