package com.hsb.giveandtakeapi.controller;

import com.hsb.giveandtakeapi.model.friend.FriendCreateRequest;
import com.hsb.giveandtakeapi.model.friend.FriendItem;
import com.hsb.giveandtakeapi.model.friend.FriendCutUpdateRequest;
import com.hsb.giveandtakeapi.model.friend.FriendResponse;
import com.hsb.giveandtakeapi.service.FriendService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/friend")
public class FriendController {
    public final FriendService friendService;

    @PostMapping("/list")
    public String setFriend(@RequestBody FriendCreateRequest request){
        friendService.setFriend(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<FriendItem> getFriends(){
        return friendService.getFriends();
    }

    @GetMapping("/detail/{id}")
    public FriendResponse getFriend(@PathVariable long id){
        return friendService.getFriend(id);
    }

    @PutMapping("/cut-update/{id}")
    public String putFriend(@PathVariable long id, @RequestBody FriendCutUpdateRequest request){
        friendService.putFriend(id, request);

        return "OK";
    }

    @PutMapping("/cut-cancel.{id}")
    public String putFriendCancel(@PathVariable long id, @RequestBody FriendCreateRequest request){
        friendService.putFriendCancel(id, request);

        return "OK";
    }
}
