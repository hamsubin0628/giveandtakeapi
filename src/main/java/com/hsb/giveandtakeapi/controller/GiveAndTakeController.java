package com.hsb.giveandtakeapi.controller;

import com.hsb.giveandtakeapi.entity.Friend;
import com.hsb.giveandtakeapi.entity.GiveAndTake;
import com.hsb.giveandtakeapi.model.friend.FriendResponse;
import com.hsb.giveandtakeapi.model.giveAndTake.GiveAndTakeCreateRequest;
import com.hsb.giveandtakeapi.model.giveAndTake.GiveAndTakeGiftPriceChangeRequest;
import com.hsb.giveandtakeapi.model.giveAndTake.GiveAndTakeItem;
import com.hsb.giveandtakeapi.model.giveAndTake.GiveAndTakeResponse;
import com.hsb.giveandtakeapi.service.FriendService;
import com.hsb.giveandtakeapi.service.GiveAndTakeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/give-and-take")
public class GiveAndTakeController {
    private final FriendService friendService;
    private final GiveAndTakeService giveAndTakeService;

    @PostMapping("/new/friend-id/{friendId}")
    public String setGiveAndTake(@PathVariable long friendId, @RequestBody GiveAndTakeCreateRequest request){
        Friend friend = friendService.getData(friendId);
        giveAndTakeService.setGiveAndTake(friend, request);

        return "OK";
    }

    @GetMapping("/all")
    public List<GiveAndTakeItem> getGiveAndTakes(){
        return giveAndTakeService.getGiveAndTakes();
    }

    @GetMapping("/detail/{id}")
    public GiveAndTakeResponse getGiveAndTake(@PathVariable long id){
        return giveAndTakeService.getGiveAndTake(id);
    }

    @PutMapping("/gift-price/{id}")
    public String putGiveAndTake(@PathVariable long id, @RequestBody GiveAndTakeGiftPriceChangeRequest request){
        giveAndTakeService.putGiveAndTake(id, request);

        return "OK";
    }
}
