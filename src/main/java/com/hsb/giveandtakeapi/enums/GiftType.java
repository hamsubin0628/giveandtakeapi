package com.hsb.giveandtakeapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GiftType {
    GIVE("줌"),
    TAKE("받음");

    private final String giftTypeEnum;
}
