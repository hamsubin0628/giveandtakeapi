package com.hsb.giveandtakeapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.cglib.core.Local;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Friend {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 13, unique = true)
    private String phoneNumber;

    @Column(nullable = false)
    private Boolean cutCheck;

    private LocalDate cutDate;

    private String cutReason;
}
