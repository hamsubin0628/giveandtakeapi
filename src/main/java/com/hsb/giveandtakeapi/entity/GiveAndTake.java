package com.hsb.giveandtakeapi.entity;

import com.hsb.giveandtakeapi.enums.GiftType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.cglib.core.Local;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class GiveAndTake {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "friendId", nullable = false)
    private Friend friend;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private GiftType giveType;

    @Column(nullable = false, length = 30)
    private String giftContent;

    @Column(nullable = false)
    private Double giftPrice;

    @Column(nullable = false)
    private LocalDate giftDate;
}
